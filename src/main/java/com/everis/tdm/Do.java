package com.everis.tdm;

import com.everis.tdm.config.AppConfig;
import com.everis.tdm.dao.avi.aviDAO;
import com.everis.tdm.model.avi.Cliente;
import com.everis.tdm.model.avi.Reclamo;
import com.everis.tdm.model.avi.Tarjeta;
import com.everis.tdm.services.AltaTarjeta;
import com.everis.tdm.services.CrearCliente;
import com.everis.tdm.web.LoyaltyStep;
import net.thucydides.core.annotations.Steps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import java.util.stream.Collectors;
import java.util.List;

import static com.everis.tdm.security.Decryption.decrypt;
import static com.everis.tdm.security.Encryption.encrypt;


public class Do {

    private static final Logger LOGGER = LoggerFactory.getLogger(Do.class);
    static AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    static aviDAO dao = context.getBean(aviDAO.class);
    @Steps
    static LoyaltyStep loyaltyStep;


    public static void setNewTarjeta(String estado, String dnicliente, String tipotarjeta) throws Exception {
        Tarjeta a = new Tarjeta();
        a.setEstado(setEncrypt(estado));
        a.setDnicliente(setEncrypt(dnicliente));
        a.setTipotarjeta(setEncrypt(setEncrypt(tipotarjeta)));

        dao.sp_insertTarjeta(a.getEstado(), a.getDnicliente(), a.getTipotarjeta());
        LOGGER.info("Registro de cliente en BD exitoso");

    }


    public static void setNewCliente(String numcelular, String tarjeta, String operador, String tipodoc, String numdoc) throws Exception {
        Cliente a = new Cliente();
        a.setNumcelular(setEncrypt(numcelular));
        a.setTarjeta(setEncrypt(tarjeta));
        a.setOperador(operador);
        a.setTipodoc(tipodoc);
        a.setNumdoc(setEncrypt(numdoc));
        dao.sp_insertCliente(a.getNumcelular(), a.getTarjeta(), a.getOperador(), a.getTipodoc(),a.getNumdoc());
        LOGGER.info("Registro de cliente en BD exitoso");

    }

    public static void updateMillas(String codigoUnico, int millasTarjeta) {
        Cliente a = new Cliente();
        //a.setCodigoUnico(codigoUnico);
        //a.setMillasTarjeta(millasTarjeta);
        //dao.sp_updateMillas(a.getCodigoUnico(), a.getMillasTarjeta());
//        LOGGER.info("Millas actualizadas en BD para el cliente {}", a.getCodigoUnico());

    }

    public static void updateRegistroBenni(String codigoUnico) {
        Cliente a = new Cliente();
   //     a.setCodigoUnico(codigoUnico);
    //    dao.sp_updateRegistroBenni(a.getCodigoUnico());
        LOGGER.info("Registro actualizado");
    }

    public static List<Cliente> getListClientes() {
        return dao.sp_getClientes();
    }
    public static List<Tarjeta> getListTarjetas() {
        return dao.sp_getTarjetaPorEstado("congelado");
    }
        public static Tarjeta getTarjetaPorEstado(String vestado)
        {
            Tarjeta tar;
            List<Tarjeta> list = dao.sp_getTarjetaPorEstado(vestado).stream().collect(Collectors.toList());
            tar = list.stream().findAny().orElse(list.stream().findAny().orElse(null));
            LOGGER.info("Cliente obtenido de TDM " + tar);
            return tar;

        }
    public static Cliente recargarMillas(Cliente cli) throws InterruptedException {
      //  LOGGER.info("Inicia la recarga de Millas para el cliente {}", cli.getCodigoUnico());

        //loyaltyStep.openLoyalty();
        //loyaltyStep.doLogin();
       // loyaltyStep.verifyLogin();
       // loyaltyStep.doMillas(cli);
        //updateMillas(cli.getCodigoUnico(), cli.getMillasTarjeta());
        return cli;
    }

    public static Cliente crearCliente(Cliente cli) {
        return CrearCliente.crearClienteBus(cli);
    }

    public static Cliente crearTarjeta(Cliente cli) {
        return AltaTarjeta.altaTarjeta(cli);
    }

    public static Cliente clienteRegistroTDM(String numcelular, String tarjeta, String operador, String tipodoc, String numdoc) throws Exception {
        Cliente cli = new Cliente();
        cli.setNumcelular(numcelular);
        cli.setTarjeta(tarjeta);
        cli.setOperador(operador);
        cli.setTipodoc(tipodoc);
        cli.setNumdoc(numdoc);
            setNewCliente(numcelular,tarjeta,operador,tipodoc,numdoc);
            return cli;
    }
    public static Tarjeta tarjetaRegistroTDM(String estado, String dnicliente, String tipotarjeta) throws Exception {
        Tarjeta tar = new Tarjeta();
        tar.setEstado(estado);
        tar.setDnicliente(dnicliente);
        tar.setTipotarjeta(tipotarjeta);

        setNewTarjeta(estado,dnicliente,tipotarjeta);
        return tar;
    }

    public static Cliente crearClienteTarjeta() throws Exception {
        Cliente cli = new Cliente();
        crearCliente(cli);
        crearTarjeta(cli);
    //    setNewCliente(cli.getTipoDocumento(), cli.getNumeroDocumento(), cli.getCodigoUnico(), cli.getTipoTarjeta(), cli.getNumeroTarjeta()
      //          , cli.getFechaCaducidadTarjeta(), 0, "0", "", "");

        return cli;

    }

    public static Cliente clienteMillasTdm() throws InterruptedException {
        Cliente cli;
      /*  List<Cliente> list = Do.getListClientes().stream().filter(Cliente::isRegistroBenni).collect(Collectors.toList());
        cli = list.stream().filter(x -> x.getMillasTarjeta() >= 10000).findAny().orElse(list.stream().findAny().orElse(null));
        if (cli == null) {
            LOGGER.info(" No existen clientes registrados, se procede a crear Cliente TDM:");
            //TODO: Crear Cliente TDM + Millas
            crearCliente(cli);
            crearTarjeta(cli);
            recargarMillas(cli);
        } else if (cli.getMillasTarjeta() < 10000) {
            LOGGER.info("El cliente tiene pocas millas");
            //recargarMillas(cli);
        }
        LOGGER.info("Cliente obtenido de TDM " + cli);

        return cli;*/
        return null;
    }


    public static String setEncrypt(String value) throws Exception {
        return encrypt(value);
    }

    public static String getDecrypt(String value) throws Exception {
        return decrypt(value);
    }

    //--------------------------------------------------------------- Renzo

    //Registrar un reclamo en la BD
    public static void setNewReclamo(String nroreclamo, String tipologia,String descripcion,String fecha,String importe,String correo) throws Exception {
        Reclamo a = new Reclamo();
        a.setNroreclamo(setEncrypt(nroreclamo));
        a.setTipologia(tipologia);
        a.setDescripcion(descripcion);
        a.setFecha(fecha);
        a.setImporte(importe);
        a.setCorreo(correo);

        dao.sp_insertReclamo(a.getNroreclamo(), a.getTipologia(), a.getDescripcion(), a.getFecha(), a.getImporte(), a.getCorreo());
        LOGGER.info("Registro de Reclamo en BD exitoso");

    }

    //Traer la lista de todos los reclamos en la BD
    public static List<Reclamo> getListReclamos() {
        return dao.sp_getReclamos();
    }

    /*public static Reclamo usuarioReclamoTDM(String tipologia, String medioRespuesta) throws Exception {
        Reclamo recl = new Reclamo();
        recl.setTipologia(tipologia);
        recl.setMedioRespuesta(medioRespuesta);
        setNewReclamo(tipologia,medioRespuesta);
        return recl;
    }*/

}
