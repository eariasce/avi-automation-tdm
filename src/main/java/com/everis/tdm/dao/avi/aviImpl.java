package com.everis.tdm.dao.avi;

import com.everis.tdm.model.avi.Cliente;

import java.util.List;

public interface aviImpl {

    void sp_insertCliente(String numcelular, String tarjeta, String operador, String tipodoc, String numdoc);

    void sp_insertTarjeta(String estado, String dnicliente, String tipotarjeta);

    List<Cliente> sp_getClientes();

}
