package com.everis.tdm.dao.avi;


import com.everis.tdm.model.avi.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class aviDAO implements aviImpl {

    public static JdbcTemplate jdbc;

    @Autowired
    public aviDAO(DataSource dataSource) {
        jdbc = new JdbcTemplate(dataSource);
    }


    public void sp_insertCliente(String numcelular, String tarjeta, String operador, String tipodoc, String numdoc) {
        String SQL = "{call insertarCliente(?,?,?,?,?)}";
        jdbc.update(SQL, numcelular,tarjeta,operador,tipodoc,numdoc);
    }
    public void sp_insertTarjeta(String estado, String dnicliente, String tipotarjeta) {
        String SQL = "{call insertarTarjeta(?,?,?)}";
        jdbc.update(SQL, estado,dnicliente,tipotarjeta);
    }

    @Override
    public List<Cliente> sp_getClientes() {
        String SQL = "{call getClientes}";
        return jdbc.query(SQL,
                new ClienteMapper());
    }



    public List<Tarjeta> sp_getTarjetaPorEstado(String vestado) {
        String SQL = "{call getTarjetaPorEstado(?)}";
        return jdbc.query(SQL, new TarjetaMapper(), new Object[]{vestado});
    }


    public void sp_updateRegistroBenni(String codigoUnico) {
        String SQL = "{call updateRegistroBenni(?)}";
        jdbc.update(SQL, codigoUnico);
    }

    //--------------------------------------------------------------- Renzo
    // llamando directamente al procedimiento almacenado
    //@Override
    public void sp_insertReclamo(String nroreclamo, String tipologia,String descripcion,String fecha,String importe,String correo) {
        String SQL = "{call insertarReclamo(?,?,?,?,?,?)}";
        jdbc.update(SQL, nroreclamo, tipologia,descripcion,fecha,importe,correo);
    }

    //@Override
    public List<Reclamo> sp_getReclamos() {
        String SQL = "{call getClientes}";
        return jdbc.query(SQL,
                new ReclamoMapper());
    }

}
