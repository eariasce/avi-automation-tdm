package com.everis.tdm.model.avi;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class TarjetaMapper implements RowMapper<Tarjeta> {
    @SneakyThrows
    public Tarjeta mapRow(ResultSet rs, int arg1) throws SQLException {
        Tarjeta data = new Tarjeta();
        data.setEstado(decrypt(rs.getString("estado")));
        data.setDnicliente(decrypt(rs.getString("dnicliente")));
        data.setTipotarjeta(rs.getString("tipotarjeta"));

        return data;
    }
}
