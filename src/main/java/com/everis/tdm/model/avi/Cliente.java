package com.everis.tdm.model.avi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Cliente {

    private String numcelular;
    private String tarjeta;
    private String operador;
    private String tipodoc;
    private String numdoc;


}
