package com.everis.tdm.model.avi;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class ReclamoMapper implements RowMapper<Reclamo> {
    @SneakyThrows
    public Reclamo mapRow(ResultSet rs, int arg1) throws SQLException {
        Reclamo data = new Reclamo();
        data.setNroreclamo(decrypt(rs.getString("nroreclamo")));
        data.setTipologia(decrypt(rs.getString("tipologia")));
        data.setDescripcion(decrypt(rs.getString("descripcion")));
        data.setDescripcion(decrypt(rs.getString("descripcion")));
        data.setFecha(decrypt(rs.getString("fecha")));
        data.setImporte(decrypt(rs.getString("importe")));
        return data;
    }
}



