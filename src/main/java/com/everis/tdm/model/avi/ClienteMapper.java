package com.everis.tdm.model.avi;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class ClienteMapper implements RowMapper<Cliente> {
    @SneakyThrows
    public Cliente mapRow(ResultSet rs, int arg1) throws SQLException {
        Cliente data = new Cliente();
        data.setNumcelular(decrypt(rs.getString("numcelular")));
        data.setTarjeta(decrypt(rs.getString("tarjeta")));
        data.setOperador(rs.getString("operador"));
        data.setTipodoc(rs.getString("tipodoc"));
        data.setNumdoc(rs.getString("numdoc"));
        return data;
    }
}