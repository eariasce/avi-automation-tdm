package com.everis.tdm.model.avi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Reclamo {

    private String nroreclamo;
    private String tipologia;
    private String descripcion;
    private String fecha;
    private String importe;
    private String correo;


}
