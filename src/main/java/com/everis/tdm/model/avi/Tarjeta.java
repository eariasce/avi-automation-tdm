package com.everis.tdm.model.avi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class Tarjeta {

    private String estado;
    private String dnicliente;
    private String tipotarjeta;


}

